import angular from 'angular';

import AppRoot from './root'
import user from './user'

const components = angular
    .module('app.components', [AppRoot, user])
    .name

export default components;