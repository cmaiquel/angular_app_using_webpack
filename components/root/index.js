import angular from 'angular';

import { AppRootComponent } from './app-root.component';

const root = angular
    .module('app.root',[])
    .component('appRoot',AppRootComponent)
    .name;

export default root;