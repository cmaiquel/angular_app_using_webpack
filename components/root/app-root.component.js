export const AppRootComponent = {
    template: `
        <app-header></app-header>
        <div ui-view></div>
        <app-footer></app-footer>
    `
}