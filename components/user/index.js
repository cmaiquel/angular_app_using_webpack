import angular from 'angular';

// Import services
import UserService from './user.service';

// Import dependencies components
import { UserItemComponent } from './user-item/user-item.component';
import { UserListComponent } from './user-list/user-list.component';

const user = angular  
    .module('users', [])
    .service('UserService', UserService)
    .component('userItem', UserItemComponent)
    .component('userList', UserListComponent)
    //.component('userDetail', UserDetailComponent)
    .config(['$stateProvider', '$urlRouterProvider', ($stateProvider, $urlRouterProvider) => {
        $stateProvider
            .state('users', {
                url: '/',
                component: 'userList',
                resolve: {
                    users: ['UserService', UserService => UserService.getUsers()]
                }
            })
            /*.state('user', {
                url: '/users/:id',
                component: 'userDetail',
                resolve: {
                    user: (UserService, $stateParams) => UserService.getUser($stateParams.id)
                }
            })*/
            $urlRouterProvider.otherwise('/')
    }])
    .name;

export default user;