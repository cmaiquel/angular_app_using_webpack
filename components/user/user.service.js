class UserService {
    
    constructor ($http) {
        this.$http = $http;
    }

    getUsers() {
        return this.$http.get('https://jsonplaceholder.typicode.com/users').then(response => response.data);
    }

    getUser(userId) {
        return this.$http.get(`https://jsonplaceholder.typicode.com/users/{$userId}`).then(response => response.data);
    }

}

UserService.$inject = ['$http'];

export default UserService;