export const UserItemComponent = {
    
    bindings: {
        userData: '<'
    },
    template: `
        <li class="collection-item avatar">
            <i class="material-icons circle green">perm_identity</i>
            <span class="title">{{$ctrl.userData.name}}</span>
            <p>{{$ctrl.userData.company.name}}<br>
                {{$ctrl.userData.phone}}
            </p>
        </li>
    `

}