export const UserListComponent = {
    bindings: {
        users: '<'
    },
    template: `
        <ul class="collection">
            <user-item ng-repeat="user in $ctrl.users" user-data="user"></user-item>
        </ul>
    `
}