// Angular & framework dependencies
import angular from 'angular';
import uiRouter from 'angular-ui-router';

// Application components
import common from './common/common';
import components from './components/components'

const root = angular
    .module('app', [
        uiRouter,
        common,
        components
    ]);

/**
 * Register main angular app
 */
document.addEventListener('DOMContentLoaded', () => angular.bootstrap(document, ['app']));

export default root;