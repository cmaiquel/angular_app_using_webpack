var gulp = require('gulp');
var webpackStream = require('webpack-stream');
var webpack = require('webpack');

gulp.task('default', function() {
  return gulp.src('./app.js')
    .pipe(webpackStream({
        output: {
            filename: 'app.js',
        },
        module: {
            loaders: [{
                test: /\.js?$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
            }]
        },
        plugins: [new webpack.optimize.UglifyJsPlugin({
            compress: {
                warnings: false
            },
            comments: false
        })]
    }))
    .pipe(gulp.dest('public/'));
});